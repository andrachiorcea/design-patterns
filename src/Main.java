import creationalPatterns.builder.meal.BuilderPatternDemo;
import creationalPatterns.factoryMethod.factory.FactoryPatternDemo;
import creationalPatterns.prototype.usePrototype.PrototypePatternDemo;

public class Main {
    public static void main(String[] args)
    {
        PrototypePatternDemo factoryPatternDemo = new PrototypePatternDemo();
        factoryPatternDemo.launch();
    }
}

