package structuralPatterns.flyweight.Model;

public interface Animal {
    public void describe();
}
