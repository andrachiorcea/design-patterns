package structuralPatterns.flyweight.Model;

public class Dog implements Animal {
    private String breed;
    private int age;

    public Dog(String breed) {
        this.breed = breed;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void describe() {
        System.out.println("Dog: [Breed: " + breed + ", Age: " + age + "];");
    }
}
