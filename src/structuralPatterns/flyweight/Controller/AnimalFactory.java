package structuralPatterns.flyweight.Controller;

import structuralPatterns.flyweight.Model.Animal;
import structuralPatterns.flyweight.Model.Dog;

import java.util.HashMap;

public class AnimalFactory {
    private static final HashMap circleMap = new HashMap();

    public static Animal getDog(String breed) {
        Dog dog = (Dog)circleMap.get(breed);

        if(dog == null) {
            dog = new Dog(breed);
            circleMap.put(breed, dog);
            System.out.println("Creating dog of breed : " + breed);
        }
        return dog;
    }
}
