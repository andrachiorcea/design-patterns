Flyweight pattern tries to reuse already existing similar kind objects by storing them and creates new
object when no matching object is found.
The Flyweight pattern describes how to share objects to allow their use at fine granularity without prohibitive cost.