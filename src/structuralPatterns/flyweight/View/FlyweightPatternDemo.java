package structuralPatterns.flyweight.View;

import structuralPatterns.flyweight.Controller.AnimalFactory;
import structuralPatterns.flyweight.Model.Dog;

public class FlyweightPatternDemo {
    private static final String breeds[] = { "A", "B", "C", "D", "E" };
    public static void main(String[] args) {

        for(int i=0; i < 10; ++i) {
            Dog dog = (Dog) AnimalFactory.getDog(getRandomBreed());
            dog.setAge(getRandomAge());
            dog.describe();
        }
    }
    private static String getRandomBreed() {
        return breeds[(int)(Math.random()*breeds.length)];
    }
    private static int getRandomAge() {
        return (int)(Math.random()*10 );
    }
}
