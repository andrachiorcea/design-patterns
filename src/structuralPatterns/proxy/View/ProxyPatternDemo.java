package structuralPatterns.proxy.View;

import structuralPatterns.proxy.Model.Check;
import structuralPatterns.proxy.Model.Payment;

public class ProxyPatternDemo {
    public static void main(String[] args) {
        Payment payment = new Check("A50NP");

        //accpunt will be created
        payment.pay();
        System.out.println("");

        //we will pay from the same account created previously
        payment.pay();
    }

}
