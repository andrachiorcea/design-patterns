package structuralPatterns.proxy.Model;

public interface Payment {
    public void pay();
}
