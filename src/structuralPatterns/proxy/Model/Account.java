package structuralPatterns.proxy.Model;

public class Account implements Payment {

    private String accountNumber;

    public Account(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public void pay() {
        System.out.println("Paying from account" + accountNumber);
    }
}
