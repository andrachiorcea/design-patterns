package structuralPatterns.proxy.Model;

public class Check implements  Payment {

    private Account account;
    private String accountNumber;

    public Check(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public void pay() {
        if(account == null){
            account = new Account(accountNumber);
        }
        account.pay();
    }
}
