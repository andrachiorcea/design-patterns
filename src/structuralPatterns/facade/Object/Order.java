package structuralPatterns.facade.Object;

public class Order {

    public Order() {
    }

    public void describe() {
        System.out.println("This is an order");
    }
}
