package structuralPatterns.facade.Object;

public class Billing extends Order{

    private int value;

    public Billing(int value) {
        this.value = value;
    }

    @Override
    public void describe() {
        System.out.println("Total cost: " + value);
    }
}
