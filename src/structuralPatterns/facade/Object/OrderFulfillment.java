package structuralPatterns.facade.Object;

public class OrderFulfillment extends Order{

    @Override
    public void describe() {
        System.out.println("The order has been registered");
    }
}
