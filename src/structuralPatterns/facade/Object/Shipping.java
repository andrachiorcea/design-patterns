package structuralPatterns.facade.Object;

public class Shipping extends Order {

    @Override
    public void describe() {
        System.out.println("We are delivering to the client ");
    }
}
