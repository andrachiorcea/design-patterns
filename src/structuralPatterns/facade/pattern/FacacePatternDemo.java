package structuralPatterns.facade.pattern;

public class FacacePatternDemo {
    public static void main(String[] args) {
        CustomerServiceFacade serviceFacade = new CustomerServiceFacade();

        serviceFacade.describeBilling();
        serviceFacade.describeOrderFulfillment();
        serviceFacade.describeShipping();
    }
}
