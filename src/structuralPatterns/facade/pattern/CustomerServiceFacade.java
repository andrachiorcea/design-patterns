package structuralPatterns.facade.pattern;

import structuralPatterns.facade.Object.Billing;
import structuralPatterns.facade.Object.Order;
import structuralPatterns.facade.Object.OrderFulfillment;
import structuralPatterns.facade.Object.Shipping;

public class CustomerServiceFacade {
    private Order billing;
    private Order orderFulfillment;
    private Order shipping;

    public CustomerServiceFacade() {
        billing = new Billing(200);
        orderFulfillment = new OrderFulfillment();
        shipping = new Shipping();
    }

    public void describeBilling(){
        billing.describe();
    }
    public void describeOrderFulfillment(){
        orderFulfillment.describe();
    }
    public void describeShipping(){
        shipping.describe();
    }
}
