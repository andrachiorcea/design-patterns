package structuralPatterns.adapter.advancedMediaPlayer;

public interface AdvancedMediaPlayer {
    public void playVlc(String fileName);
    public void playMp4(String fileName);
}
