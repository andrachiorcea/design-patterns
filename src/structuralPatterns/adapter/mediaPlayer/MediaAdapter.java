package structuralPatterns.adapter.mediaPlayer;

import structuralPatterns.adapter.advancedMediaPlayer.AdvancedMediaPlayer;
import structuralPatterns.adapter.advancedMediaPlayer.Mp4Player;
import structuralPatterns.adapter.advancedMediaPlayer.VlcPlayer;

public class MediaAdapter implements  MediaPlayer {
    AdvancedMediaPlayer advancedMusicPlayer;

    public MediaAdapter(String audioType){

        if(audioType.equalsIgnoreCase("vlc") ){
            advancedMusicPlayer = new VlcPlayer();

        }else if (audioType.equalsIgnoreCase("mp4")){
            advancedMusicPlayer = new Mp4Player();
        }
    }

    @Override
    public void play(String audioType, String fileName) {

        if(audioType.equalsIgnoreCase("vlc")){
            advancedMusicPlayer.playVlc(fileName);
        }
        else if(audioType.equalsIgnoreCase("mp4")){
            advancedMusicPlayer.playMp4(fileName);
        }
    }
}
