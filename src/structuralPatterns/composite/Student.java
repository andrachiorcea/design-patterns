package structuralPatterns.composite;

import java.util.ArrayList;
import java.util.List;

public class Student {
    private String name;
    private String group;
    private int year;
    private List<Student> subordinates;

    // constructor
    public Student(String name,String group, int year) {
        this.name = name;
        this.group = group;
        this.year = year;
        subordinates = new ArrayList<Student>();
    }

    public void add(Student e) {
        subordinates.add(e);
    }

    public void remove(Student e) {
        subordinates.remove(e);
    }

    public List<Student> getSubordinates(){
        return subordinates;
    }

    public String toString(){
        return ("Student :[ Name : " + name + ", group : " + group + ", year :" + year+" ]");
    }
}
