package structuralPatterns.composite;

public class CompositePatternDemo {
    public static void main(String[] args) {

        Student sefDeAn = new Student("Ana","A5", 3);

        Student sefDeGrupa = new Student("Robert","B7", 3);

        Student studentSimplu1 = new Student("Andreea","B7", 3);
        Student studentSimplu2 = new Student("Unu","E", 3);
        Student studentSimplu3 = new Student("Mihai","E", 3);

        Student studentSimplu4 = new Student("Andreea","E", 3);
        Student studentSimplu5 = new Student("Altu","E", 3);

        sefDeAn.add(studentSimplu1);
        sefDeAn.add(studentSimplu2);
        sefDeAn.add(studentSimplu3);
        sefDeAn.add(studentSimplu4);
        sefDeAn.add(studentSimplu5);

        sefDeGrupa.add(studentSimplu1);
        sefDeGrupa.add(studentSimplu2);
        sefDeGrupa.add(studentSimplu3);

        //print all employees of the organization
        System.out.println(sefDeAn);

        for (Student student : sefDeAn.getSubordinates()) {
            System.out.println(student);

            for (Student student1 : student.getSubordinates()) {
                System.out.println(student1);
            }
        }
    }
}
