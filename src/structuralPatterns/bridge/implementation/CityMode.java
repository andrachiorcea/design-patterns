package structuralPatterns.bridge.implementation;

public class CityMode implements Drive {
    @Override
    public void driveCar(String carName) {
        System.out.println("Drivine car " + carName + " in city mode" );
    }
}
