package structuralPatterns.bridge.implementation;

public interface Drive {
    public void driveCar(String carName);
}
