package structuralPatterns.bridge.implementation;

public class SportMode implements Drive {
    @Override
    public void driveCar(String carName) {
        System.out.println("Drivine car " + carName + " in sports mode" );
    }
}
