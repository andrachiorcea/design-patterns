package structuralPatterns.bridge.abstraction;

import structuralPatterns.bridge.implementation.Drive;

public class Car extends Automobile {

    private String carName;

    public Car(Drive drive, String carName) {
        super(drive);
        this.carName = carName;
    }

    public void drive() {
        drive.driveCar(carName);
    }
}
