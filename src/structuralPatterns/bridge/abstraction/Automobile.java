package structuralPatterns.bridge.abstraction;

import structuralPatterns.bridge.implementation.Drive;

public abstract class Automobile {
    protected Drive drive;

    protected Automobile(Drive drive){
        this.drive = drive;
    }
    public abstract void drive();
}
