package structuralPatterns.bridge;

import structuralPatterns.bridge.abstraction.Car;
import structuralPatterns.bridge.implementation.CityMode;
import structuralPatterns.bridge.implementation.SportMode;

public class BridgePatternDemo {
    public static void main(String[] args) {
        Car car1 = new Car(new SportMode(), "Range Rover");
        Car car2 = new Car(new CityMode(), "Opel");

        car1.drive();
        car2.drive();
    }
}
