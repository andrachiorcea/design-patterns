package structuralPatterns.decorator.decoratedObject;

public interface Tree {
    void show();
}
