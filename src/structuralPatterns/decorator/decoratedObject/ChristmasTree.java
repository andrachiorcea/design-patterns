package structuralPatterns.decorator.decoratedObject;

public class ChristmasTree implements Tree {
    public void show() {
        System.out.println("This is a tree");
    }
}
