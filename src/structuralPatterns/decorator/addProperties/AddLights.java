package structuralPatterns.decorator.addProperties;

import structuralPatterns.decorator.decoratedObject.Tree;

public class AddLights extends ChristmasTreeDecorator {

    public AddLights(Tree tree) {
        super(tree);
    }

    @Override
    public void show() {
        tree.show();
        getLights(tree);
    }

    private void getLights(Tree tree){
        System.out.println("Congratulations, you just added neon lights!");
    }
}
