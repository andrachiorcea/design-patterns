package structuralPatterns.decorator.addProperties;

import structuralPatterns.decorator.decoratedObject.Tree;

public abstract class ChristmasTreeDecorator implements Tree
{
    protected Tree tree;

    public ChristmasTreeDecorator(Tree tree){
        this.tree = tree;
    }

    public void lightUp(){
        tree.show();
    }
}
