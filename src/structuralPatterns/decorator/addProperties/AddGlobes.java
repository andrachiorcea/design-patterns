package structuralPatterns.decorator.addProperties;

import structuralPatterns.decorator.decoratedObject.Tree;

public class AddGlobes extends ChristmasTreeDecorator {
    public AddGlobes(Tree tree) {
        super(tree);
    }

    @Override
    public void show() {
        tree.show();
        getGlobes(tree);
    }

    private void getGlobes(Tree tree){
        System.out.println("Congratulations, you just added some globes!");
    }
}
