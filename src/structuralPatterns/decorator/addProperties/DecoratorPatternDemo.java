package structuralPatterns.decorator.addProperties;

import structuralPatterns.decorator.decoratedObject.ChristmasTree;
import structuralPatterns.decorator.decoratedObject.Tree;

public class DecoratorPatternDemo {
    public static void main(String[] args) {

        ChristmasTree circle = new ChristmasTree();

        Tree treeWithLights = new AddLights(new ChristmasTree());
        Tree treeWithGlobes = new AddGlobes(new ChristmasTree());
        System.out.println("Empty tree");
        circle.show();

        System.out.println("Tree with lights");
        treeWithLights.show();

        System.out.println("Tree with globes");
        treeWithGlobes.show();
    }
}
