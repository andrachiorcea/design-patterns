package behavioralPatterns.interpreter.context;

import behavioralPatterns.interpreter.expressions.Divide;
import behavioralPatterns.interpreter.expressions.Expression;
import behavioralPatterns.interpreter.expressions.Modulo;
import behavioralPatterns.interpreter.expressions.Multiply;

public class Interpreter {
    private Context context;

    public Interpreter(Context context) {
        this.context = context;
    }

    public int interpret(String input) {

        Expression exp = null;

        if(input.contains("multiply")) {
            exp = new Multiply(input);
        } else if(input.contains("divide")) {
            exp = new Divide(input);
        } else if(input.contains("modulo")) {
            exp = new Modulo(input);
        }

        int result = exp.interpret(context);
        System.out.print(input);

        return result;
    }
}
