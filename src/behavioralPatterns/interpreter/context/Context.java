package behavioralPatterns.interpreter.context;

public class Context {
    public int multiply(String input) {
        String[] numbers = interpret(input);
        int m=1;
        for(String t:numbers){
            m=m*Integer.parseInt(t);
        }
        return (m);
    }

    public int divide(String input) {
        String[] numbers = interpret(input);
        int num1 = Integer.parseInt(numbers[0]);
        int num2 = Integer.parseInt(numbers[1]);
        return (num1 / num2);
    }

    public int modulo(String input) {
        String[] numbers = interpret(input);
        int num1 = Integer.parseInt(numbers[0]);
        int num2 = Integer.parseInt(numbers[1]);
        return (num1 % num2);
    }

    private String[] interpret(String input) {
        String str = input.replaceAll("[^0-9 ]", "").trim();
        String[] numbers = str.split("( )+");
        return numbers;
    }
}
