package behavioralPatterns.interpreter.context;

public class InterpreterPatternDemo {
    public void launch(){
        Interpreter interpreter = new Interpreter(new Context());
        System.out.println("= " + interpreter.interpret("multiply 12 and 13 and 2"));
        System.out.println("= " + interpreter.interpret("620 modulo 3"));
    }
}
