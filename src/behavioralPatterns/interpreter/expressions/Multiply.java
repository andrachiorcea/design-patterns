package behavioralPatterns.interpreter.expressions;

import behavioralPatterns.interpreter.context.Context;

public class Multiply implements Expression{
    private String expression;

    public Multiply(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(Context context) {
        return context.multiply(expression);
    }

}

