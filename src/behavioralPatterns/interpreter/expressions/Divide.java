package behavioralPatterns.interpreter.expressions;

import behavioralPatterns.interpreter.context.Context;

public class Divide implements Expression{
    private String expression;

    public Divide(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(Context context) {
        return context.divide(expression);
    }
}
