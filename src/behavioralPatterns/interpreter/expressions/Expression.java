package behavioralPatterns.interpreter.expressions;

import behavioralPatterns.interpreter.context.Context;

public interface Expression {
    public int interpret(Context context);
}
