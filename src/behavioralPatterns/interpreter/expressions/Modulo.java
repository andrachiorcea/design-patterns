package behavioralPatterns.interpreter.expressions;

import behavioralPatterns.interpreter.context.Context;

public class Modulo implements Expression{
    private String expression;

    public Modulo(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(Context context) {
        return context.modulo(expression);
    }
}
