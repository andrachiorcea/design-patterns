package behavioralPatterns.templateMethod;

public class TemplateMethodPatternDemo {
    public void launch(){
        Cake cake=new VanillaCake();
        cake.bake();
        cake=new ChocolateCake();
        cake.bake();
    }
}
