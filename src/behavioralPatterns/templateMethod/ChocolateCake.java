package behavioralPatterns.templateMethod;

public class ChocolateCake  extends Cake {
    @Override
    void makeBatter() {
        System.out.println("Added cocoa powder to the batter");
    }

    @Override
    void makeFrosting() {
        System.out.println("Added nutella to the frosting");
    }
}
