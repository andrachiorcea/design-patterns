package behavioralPatterns.templateMethod;

public class VanillaCake extends Cake {
    @Override
    void makeBatter() {
        System.out.println("Added vanilla beans to the batter");
    }

    @Override
    void makeFrosting() {
        System.out.println("Added vanilla extract to the frosting");
    }
}
