package behavioralPatterns.templateMethod;

public abstract class Cake {
    public final void bake() {
        makeBatter();
        makeFrosting();
        decorate();
        System.out.println("The cake was assembled");
    }
    abstract void makeBatter();
    abstract void makeFrosting();
    private void decorate(){
        System.out.println("Added fondant and fruit");
    }

}
