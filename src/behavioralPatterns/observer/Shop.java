package behavioralPatterns.observer;

public interface Shop {
    public void update(Object o);
}
