package behavioralPatterns.observer;

public class Supermarket implements Shop{
    private String product;

    @Override
    public void update(Object product) {
        this.setProduct((String) product);
    }

    public void setProduct(String product){
        this.product=product;
    }

    public String getProduct(){
        return product;
    }
}
