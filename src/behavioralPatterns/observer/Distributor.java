package behavioralPatterns.observer;

import java.util.ArrayList;
import java.util.List;

public class Distributor {
    private String product;
    private List<Shop> shops = new ArrayList<>();

    public void addObserver(Shop shop) {
        this.shops.add(shop);
    }

    public void removeObserver(Shop shop) {
        this.shops.remove(shop);
    }

    public void notifyNewProduct(String product) {
        this.product = product;
        for (Shop shop : this.shops) {
            shop.update(this.product);
        }
    }

    public String getProduct(){
        return product;
    }

    public void launch(){
        Supermarket observer = new Supermarket();

        this.addObserver(observer);
        this.notifyNewProduct("skincare");
        System.out.println(this.getProduct());
    }
}
