package behavioralPatterns.visitor.visitor;

import behavioralPatterns.visitor.pets.*;

public interface Visitor {
    void visit(Cat cat);
    void visit(Dog dog);
    void visit(Litter litter);
}
