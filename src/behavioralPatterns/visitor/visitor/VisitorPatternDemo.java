package behavioralPatterns.visitor.visitor;

import behavioralPatterns.visitor.pets.Litter;

public class VisitorPatternDemo {
    public void launch(){
        Litter litter=new Litter();
        litter.accept(new PetVisitor());
    }
}
