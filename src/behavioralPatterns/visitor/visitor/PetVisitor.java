package behavioralPatterns.visitor.visitor;

import behavioralPatterns.visitor.pets.*;

public class PetVisitor implements Visitor{
    @Override
    public void visit(Cat cat) {
        System.out.println("A cat was snuggled.");
    }

    @Override
    public void visit(Dog dog) {
        System.out.println("A dog was snuggled.");
    }

    @Override
    public void visit(Litter litter){
        System.out.println("A litter of pets was snuggled.");
    }
}
