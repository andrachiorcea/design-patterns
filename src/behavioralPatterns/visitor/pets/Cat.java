package behavioralPatterns.visitor.pets;

import behavioralPatterns.visitor.visitor.PetVisitor;

public class Cat implements Pet{
    @Override
    public void accept(PetVisitor petVisitor) {
        petVisitor.visit(this);
    }
}
