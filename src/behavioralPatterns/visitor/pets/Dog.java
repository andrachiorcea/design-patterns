package behavioralPatterns.visitor.pets;

import behavioralPatterns.visitor.visitor.PetVisitor;

public class Dog implements Pet{
    @Override
    public void accept(PetVisitor petVisitor) {
        petVisitor.visit(this);
    }
}
