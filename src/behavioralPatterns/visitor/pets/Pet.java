package behavioralPatterns.visitor.pets;

import behavioralPatterns.visitor.visitor.*;

public interface Pet {
    public void accept(PetVisitor petVisitor);
}
