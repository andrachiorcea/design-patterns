package behavioralPatterns.visitor.pets;

import behavioralPatterns.visitor.visitor.PetVisitor;

public class Litter implements Pet{
    Pet[] pets;
    public Litter(){
        pets=new Pet[]{new Cat(),new Dog()};
    }

    @Override
    public void accept(PetVisitor petVisitor){
        for(Pet pet:pets){
            pet.accept(petVisitor);
        }
        petVisitor.visit(this);
    }
}
