package behavioralPatterns.memento;

public class Originator {
    private String state;

    public void setState(String state) {
        System.out.println("State was set to " + state);
        this.state = state;
    }

    public Memento save() {
        System.out.println("State saved.");
        return new Memento(state);
    }
    public void restore(Memento m) {
        state = m.getState();
        System.out.println("Restored to " + state);
    }
}
