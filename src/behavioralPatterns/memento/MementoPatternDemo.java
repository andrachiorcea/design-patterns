package behavioralPatterns.memento;

public class MementoPatternDemo {
    public void launch(){
        Caretaker caretaker = new Caretaker();
        Originator originator = new Originator();
        originator.setState("1");
        originator.setState("state 2");
        caretaker.addMemento( originator.save() );
        originator.setState("state 4");
        originator.restore(caretaker.getMemento());
        originator.setState("state 8");
        originator.restore( caretaker.getMemento() );
    }
}
