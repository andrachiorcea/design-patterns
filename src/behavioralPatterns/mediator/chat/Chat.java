package behavioralPatterns.mediator.chat;

import behavioralPatterns.mediator.user.*;

public interface Chat {
    public void sendMessage(User user, String msg);
    void addUser(User user);
    void removeUser(User user);
}
