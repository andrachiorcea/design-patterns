package behavioralPatterns.mediator.chat;

import behavioralPatterns.mediator.user.ConversationPartner;
import behavioralPatterns.mediator.user.User;

public class MediatorPatternDemo {
    public void launch(){
        Chat mediator = new ChatRoom();
        User user1 = new ConversationPartner(mediator, "Andreea");
        User user2 = new ConversationPartner(mediator, "Andrew");
        User user3 = new ConversationPartner(mediator, "Scott");
        User user4 = new ConversationPartner(mediator, "Stacy");
        mediator.addUser(user1);
        mediator.addUser(user2);
        mediator.addUser(user3);
        mediator.addUser(user4);
        user1.send("Hello");
        mediator.removeUser(user2);
        user3.send("Hello everybody");
    }
}
