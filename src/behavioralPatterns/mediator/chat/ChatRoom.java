package behavioralPatterns.mediator.chat;

import behavioralPatterns.mediator.user.User;
import java.util.*;

public class ChatRoom implements Chat {
    private List<User> users;

    public ChatRoom(){
        this.users=new ArrayList<>();
    }

    @Override
    public void addUser(User user){
        this.users.add(user);
    }

    @Override
    public void removeUser(User user){
        for(User u : this.users){
            //message should not be received by the user sending it
            if(u != user){
                System.out.println(u.getName()+ ": "+user.getName()+ " has left the conversation");
            }
        }
        this.users.remove(user);
    }

    @Override
    public void sendMessage(User user, String msg) {
        for(User u : this.users){
            //message should not be received by the user sending it
            if(u != user){
                u.receive(msg);
            }
        }
    }
}
