package behavioralPatterns.mediator.user;

import behavioralPatterns.mediator.chat.Chat;

public abstract class User {
    protected Chat mediator;
    protected String name;

    public String getName(){
        return name;
    }

    public User(Chat mediator, String name){
        this.mediator=mediator;
        this.name=name;
    }

    public abstract void send(String msg);

    public abstract void receive(String msg);
}
