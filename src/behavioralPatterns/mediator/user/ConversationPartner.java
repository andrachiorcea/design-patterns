package behavioralPatterns.mediator.user;

import behavioralPatterns.mediator.chat.Chat;

public class ConversationPartner extends User {
    public ConversationPartner(Chat mediator, String name) {
        super(mediator, name);
    }

    @Override
    public void send(String msg){
        System.out.println(this.name+": Sending Message: "+msg);
        mediator.sendMessage(this,msg);
    }
    @Override
    public void receive(String msg) {
        System.out.println(this.name+": Received Message: "+msg);
    }
}
