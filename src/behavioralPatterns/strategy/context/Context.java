package behavioralPatterns.strategy.context;

import behavioralPatterns.strategy.operations.*;

public class Context {
    private Operation operation;

    public Context(Operation operation){
        this.operation = operation;
    }

    public String executeStrategy(String string1, String string2){
        return operation.doOperation(string1, string2);
    }
}
