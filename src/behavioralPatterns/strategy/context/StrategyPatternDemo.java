package behavioralPatterns.strategy.context;

import behavioralPatterns.strategy.operations.*;

public class StrategyPatternDemo {
    public void launch(){
        Context context = new Context(new Concatenation());
        System.out.println("abs + tract = " + context.executeStrategy("abs", "tract"));

        context = new Context(new Equality());
        System.out.println("ten equals ten = " + context.executeStrategy("ten", "ten"));

        context = new Context(new EndsWith());
        System.out.println("abstract ends with tract = " + context.executeStrategy("abstract", "tract"));
    }
}
