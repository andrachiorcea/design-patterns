package behavioralPatterns.strategy.operations;

public class EndsWith implements Operation{
    @Override
    public String doOperation(String string1, String string2) {
        return string1.endsWith(string2)?"true":"false";
    }
}
