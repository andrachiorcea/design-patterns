package behavioralPatterns.strategy.operations;

public class Equality implements Operation{
    @Override
    public String doOperation(String string1, String string2) {
        return string1.equals(string2)? "true" :"false";
    }
}
