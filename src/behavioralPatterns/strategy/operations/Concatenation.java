package behavioralPatterns.strategy.operations;

public class Concatenation implements Operation{
    @Override
    public String doOperation(String string1, String string2) {
        return string1+string2;
    }
}
