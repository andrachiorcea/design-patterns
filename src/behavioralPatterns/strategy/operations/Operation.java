package behavioralPatterns.strategy.operations;

public interface Operation {
    public String doOperation(String string1, String string2);
}
