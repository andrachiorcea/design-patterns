package behavioralPatterns.state.context;

import behavioralPatterns.state.states.State;

public class PlayerContext {
    private State state;
    public PlayerContext() {
        this.state= null;
    }
    public void play() {
        state.play(this);
    }
    public void setState(State state) {
        this.state = state;
    }
    public State getState() {
        return state;
    }
}
