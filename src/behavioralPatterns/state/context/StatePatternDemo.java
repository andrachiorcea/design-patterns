package behavioralPatterns.state.context;

import behavioralPatterns.state.states.*;

public class StatePatternDemo {
    public void launch(){
        PlayerContext context = new PlayerContext();
        PauseState startState = new PauseState();
        startState.play(context);
        PlayState pauseState = new PlayState();
        pauseState.play(context);
    }
}

