package behavioralPatterns.state.states;

import behavioralPatterns.state.context.PlayerContext;

public interface State {
    void play(PlayerContext context);
}
