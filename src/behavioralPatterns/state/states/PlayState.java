package behavioralPatterns.state.states;

import behavioralPatterns.state.context.PlayerContext;

public class PlayState implements State{

    public void play(PlayerContext context) {
        System.out.println("Player has been paused.");
        context.setState(new PauseState());
    }
}
