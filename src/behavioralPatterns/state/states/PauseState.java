package behavioralPatterns.state.states;

import behavioralPatterns.state.context.*;

public class PauseState implements State{

    public void play(PlayerContext context) {
        System.out.println("Player has been unpaused or started.");
        context.setState(new PlayState());
    }
}
