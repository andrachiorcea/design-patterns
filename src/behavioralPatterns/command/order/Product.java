package behavioralPatterns.command.order;

public class Product {
    private String name;
    private int quantity;

    public Product(String name, int quantity){
        this.name=name;
        this.quantity=quantity;
    }
    public void buyProduct(){
        System.out.println("Customer bought " + quantity +" "+name+"s");
    }
    public void returnProduct(){
        System.out.println("Customer returned " + quantity +" "+name+"s");
    }
}
