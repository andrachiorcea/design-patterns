package behavioralPatterns.command.order;
import behavioralPatterns.command.commands.*;

public class CommandPatternDemo {
    public void launch(){
        Product pencil = new Product("pencil",10);
        Product ruler = new Product("ruler",5);

        BuyProduct toBuy = new BuyProduct(pencil);
        ReturnProduct toReturn = new ReturnProduct(ruler);

        CommandInvoker broker = new CommandInvoker();
        broker.takeOrder(toBuy);
        broker.takeOrder(toReturn);

        broker.placeOrders();
    }
}
