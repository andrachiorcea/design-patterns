package behavioralPatterns.command.order;

import behavioralPatterns.command.commands.*;
import java.util.*;
public class CommandInvoker {
    private List<Command> orderList = new ArrayList<>();
    public void takeOrder(Command order){
        orderList.add(order);
    }

    public void placeOrders(){
        for (Command order : orderList) {
            order.execute();
        }
        orderList.clear();
    }
}