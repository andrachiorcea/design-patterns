package behavioralPatterns.command.commands;

import behavioralPatterns.command.order.Product;

public class BuyProduct implements Command {
    private Product product;

    public BuyProduct(Product product){
        this.product = product;
    }

    public void execute() {
        product.buyProduct();
    }
}
