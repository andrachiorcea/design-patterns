package behavioralPatterns.command.commands;
import behavioralPatterns.command.order.Product;

public class ReturnProduct implements Command {
    private Product product;

    public ReturnProduct(Product product){
        this.product = product;
    }

    public void execute() {
        product.returnProduct();
    }
}
