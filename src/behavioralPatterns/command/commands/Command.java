package behavioralPatterns.command.commands;

public interface Command {
    void execute();
}
