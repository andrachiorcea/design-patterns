package behavioralPatterns.chainOfResponsibility.atm;
import behavioralPatterns.chainOfResponsibility.dispenser.*;

public class ATM {

    private DispenseChain c1;

    public ATM() {
        this.c1 = new FiftyDollarDispenser();
        DispenseChain c2 = new TwentyDollarDispenser();
        DispenseChain c3 = new TenDollarDispenser();
        c1.setNextChain(c2);
        c2.setNextChain(c3);
    }
    public void dispense(int amount){
        System.out.println("Amount to dispense: "+amount);
            if (amount % 10 != 0) {
                System.out.println("Please enter a multiple of 10.");
                return;
            }
            c1.dispense(new Credit(amount));
    }

}