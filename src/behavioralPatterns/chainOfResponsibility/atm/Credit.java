package behavioralPatterns.chainOfResponsibility.atm;

public class Credit {

    private int amount;

    public Credit(int amount){
        this.amount=amount;
    }

    public int getAmount(){
        return this.amount;
    }
}