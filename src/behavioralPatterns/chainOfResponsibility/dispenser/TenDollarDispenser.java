package behavioralPatterns.chainOfResponsibility.dispenser;
import behavioralPatterns.chainOfResponsibility.atm.*;

public class TenDollarDispenser implements DispenseChain {

    private DispenseChain chain;

    @Override
    public void setNextChain(DispenseChain nextChain) {
        this.chain=nextChain;
    }

    @Override
    public void dispense(Credit amount) {
        if(amount.getAmount() >= 10){
            int num = amount.getAmount()/10;
            int remainder = amount.getAmount() % 10;
            System.out.println("Received "+num+" 10$ note(s)");
            if(remainder !=0)
            {
                this.chain.dispense(new Credit(remainder));
            }
        }else{
            this.chain.dispense(amount);
        }
    }

}