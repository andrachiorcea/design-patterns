package behavioralPatterns.chainOfResponsibility.dispenser;
import behavioralPatterns.chainOfResponsibility.atm.*;

public class FiftyDollarDispenser implements DispenseChain {

    private DispenseChain chain;

    @Override
    public void setNextChain(DispenseChain nextChain) {
        this.chain=nextChain;
    }

    @Override
    public void dispense(Credit amount) {
        if(amount.getAmount() >= 50){
            int num = amount.getAmount()/50;
            int remainder = amount.getAmount() % 50;
            System.out.println("Received "+num+" 50$ note(s)");
            if(remainder !=0)
            {
                this.chain.dispense(new Credit(remainder));
            }
        }else{
            this.chain.dispense(amount);
        }
    }

}