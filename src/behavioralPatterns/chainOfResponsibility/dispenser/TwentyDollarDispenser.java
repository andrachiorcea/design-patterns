package behavioralPatterns.chainOfResponsibility.dispenser;

import behavioralPatterns.chainOfResponsibility.atm.*;

public class TwentyDollarDispenser implements DispenseChain {

    private DispenseChain chain;

    @Override
    public void setNextChain(DispenseChain nextChain) {
        this.chain=nextChain;
    }

    @Override
    public void dispense(Credit amount) {
        if(amount.getAmount() >= 20){
            int num = amount.getAmount()/20;
            int remainder = amount.getAmount() % 20;
            System.out.println("Received "+num+" 20$ note(s)");
            if(remainder !=0)
            {
                this.chain.dispense(new Credit(remainder));
            }
        }else{
            this.chain.dispense(amount);
        }
    }

}