package behavioralPatterns.chainOfResponsibility.dispenser;
import behavioralPatterns.chainOfResponsibility.atm.Credit;

public interface DispenseChain {

    void setNextChain(DispenseChain nextChain);

    void dispense(Credit amount);
}