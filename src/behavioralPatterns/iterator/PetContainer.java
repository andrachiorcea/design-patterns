package behavioralPatterns.iterator;

public class PetContainer implements Container{
    public String pets[] = {"rabbit" , "cat" ,"dog" , "hamster"};

    @Override
    public Iterator getIterator() {
        return new PetIterator();
    }

    private class PetIterator implements Iterator{

        int index;

        @Override
        public boolean hasNext() {

            if(index < pets.length){
                return true;
            }
            return false;
        }

        @Override
        public Object next() {

            if (this.hasNext()) {
                return pets[index++];
            }
            return null;
        }
    }
    public void launch(){
        for(Iterator iter = this.getIterator(); iter.hasNext();){
            String name = (String)iter.next();
            System.out.println("Name : " + name);
        }
    }
}
