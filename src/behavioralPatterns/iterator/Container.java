package behavioralPatterns.iterator;

public interface Container {
    public Iterator getIterator();
}
