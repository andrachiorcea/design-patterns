package creationalPatterns.factoryMethod.factory;

import creationalPatterns.factoryMethod.icecream.Icecream;

public class FactoryPatternDemo {
    public void launch() {
        IcecreamFactory icecreamFactory= new IcecreamFactory();
        Icecream icecream1 = icecreamFactory.getFlavour("vanilla");
        Icecream icecream2 = icecreamFactory.getFlavour("mint");
        Icecream icecream3 = icecreamFactory.getFlavour("chocolate");
        icecream1.taste();
        icecream2.taste();
        icecream3.taste();
    }
}
