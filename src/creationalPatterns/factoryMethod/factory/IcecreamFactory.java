package creationalPatterns.factoryMethod.factory;

import creationalPatterns.factoryMethod.icecream.ChocolateIcecream;
import creationalPatterns.factoryMethod.icecream.Icecream;
import creationalPatterns.factoryMethod.icecream.MintIcecream;
import creationalPatterns.factoryMethod.icecream.VanillaIcecream;

public class IcecreamFactory {
    public Icecream getFlavour(String flavour) {
        if (flavour.equalsIgnoreCase("vanilla")) {
            return new VanillaIcecream();
        }
        else if (flavour.equalsIgnoreCase("chocolate")) {
            return new ChocolateIcecream();
        }
        else if (flavour.equalsIgnoreCase("mint")) {
            return new MintIcecream();
        }
        return null;
    }
}
