package creationalPatterns.factoryMethod.icecream;

public class MintIcecream implements Icecream {
    @Override
    public void taste() {
        System.out.println("It tastes like mint");
    }
}
