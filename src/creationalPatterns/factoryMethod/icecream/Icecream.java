package creationalPatterns.factoryMethod.icecream;

public interface Icecream {
    void taste();
}
