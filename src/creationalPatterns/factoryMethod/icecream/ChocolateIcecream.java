package creationalPatterns.factoryMethod.icecream;

public class ChocolateIcecream implements Icecream{
    @Override
    public void taste() {
        System.out.println("It tastes like chocolate");
    }
}
