package creationalPatterns.factoryMethod.icecream;

public class VanillaIcecream implements Icecream {
    @Override
    public void taste() {
        System.out.println("It tastes like vanilla");
    }
}
