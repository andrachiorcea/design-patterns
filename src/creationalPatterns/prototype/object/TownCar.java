package creationalPatterns.prototype.object;

public class TownCar extends Car {

    public TownCar() {
        this.setModel("TownCar");
    }

    @Override
    void build() {
        System.out.println("This is a town car");
    }
}
