package creationalPatterns.prototype.object;

public class SportsCar extends Car {

    public SportsCar() {
        this.setModel( "SportsCar");
    }

    @Override
    void build() {
        System.out.println("This is a sports car");
    }
}
