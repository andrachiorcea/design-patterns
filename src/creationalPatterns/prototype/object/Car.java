package creationalPatterns.prototype.object;

public abstract class Car implements Cloneable {

    private String model;
    private String brand;
    private String id;

    abstract void build();

    public String getModel() {
        return model;
    }

    public String getBrand() {
        return brand;
    }

    public String getId() {
        return id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Object clone() {
        Object clone = null;

        try {
            clone = super.clone();

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return clone;
    }
}
