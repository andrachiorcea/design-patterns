package creationalPatterns.prototype.usePrototype;

import creationalPatterns.prototype.object.Car;
import creationalPatterns.prototype.object.SportsCar;
import creationalPatterns.prototype.object.TownCar;

import java.util.Hashtable;

public class CarCache {
    private static Hashtable<String, Car> shapeMap  = new Hashtable<String, Car>();

    public static Car getShape(String shapeId) {
        Car cachedShape = shapeMap.get(shapeId);
        return (Car) cachedShape.clone();
    }

    public static void loadCache() {
        SportsCar sport = new SportsCar();
        sport.setId("1");
        shapeMap.put(sport.getId(),sport);

        TownCar town = new TownCar();
        town.setId("2");
        shapeMap.put(town.getId(),town);
    }
}
