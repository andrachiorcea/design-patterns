package creationalPatterns.prototype.usePrototype;

import creationalPatterns.prototype.object.Car;

public class PrototypePatternDemo {
    public void launch() {
        CarCache.loadCache();

        Car clone1 = CarCache.getShape("1");
        Car clone2 = CarCache.getShape("2");

        System.out.println(clone1.getModel());
        System.out.println(clone2.getModel());
    }
}
