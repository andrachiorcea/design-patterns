package creationalPatterns.abstractFactory.factory;

import creationalPatterns.abstractFactory.toppings.Topping;
import creationalPatterns.abstractFactory.vegetable.Vegetable;

public class AbstractFactoryPatternDemo {
    public void launch() {

        System.out.println("Your sandwich has the following ingredients");

        AbstractFactory shapeFactory = FactoryProducer.getFactory("topping");

        Topping topping1 = shapeFactory.getTopping("ketchup");
        topping1.pick();

        Topping topping2 = shapeFactory.getTopping("mayo");
        topping2.pick();

        Topping topping3 = shapeFactory.getTopping("yoghurt");
        topping3.pick();

        AbstractFactory colorFactory = FactoryProducer.getFactory("vegetable");
        Vegetable vegetable1 = colorFactory.getVegetable("olive");

        vegetable1.choose();
        Vegetable vegetable2 = colorFactory.getVegetable("pickles");

        vegetable2.choose();
        Vegetable vegetable3 = colorFactory.getVegetable("tomato");

        vegetable3.choose();
    }
}
