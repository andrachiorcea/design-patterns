package creationalPatterns.abstractFactory.factory;

public class FactoryProducer {
    public static AbstractFactory getFactory(String choice){

        if(choice.equalsIgnoreCase("topping")){
            return new ToppingFactory();

        }else if(choice.equalsIgnoreCase("vegetable")){
            return new VegetableFactory();
        }

        return null;
    }
}
