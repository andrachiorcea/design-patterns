package creationalPatterns.abstractFactory.factory;

import creationalPatterns.abstractFactory.toppings.Topping;
import creationalPatterns.abstractFactory.vegetable.Vegetable;


public abstract class AbstractFactory {
    abstract Topping getTopping(String topping);
    abstract Vegetable getVegetable(String vegetable) ;
}