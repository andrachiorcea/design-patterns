package creationalPatterns.abstractFactory.factory;

import creationalPatterns.abstractFactory.toppings.*;
import creationalPatterns.abstractFactory.vegetable.*;


public class ToppingFactory extends AbstractFactory {

    @Override
    public Topping getTopping(String topping) {
        if(topping == null) {
            return null;
        }
        else if (topping.equalsIgnoreCase("ketchup")) {
            return new Ketchup();
        }
        else if (topping.equalsIgnoreCase("mayo")) {
            return new Mayo();
        }
        else if (topping.equalsIgnoreCase("yoghurt")) {
            return new Yoghurt();
        }
        return null;
    }

    @Override
    Vegetable getVegetable(String vegetable) {
        return null;
    }
}
