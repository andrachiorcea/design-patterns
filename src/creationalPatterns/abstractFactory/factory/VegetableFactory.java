package creationalPatterns.abstractFactory.factory;

import creationalPatterns.abstractFactory.toppings.*;
import creationalPatterns.abstractFactory.vegetable.*;

public class VegetableFactory extends AbstractFactory{

    @Override
    public Vegetable getVegetable(String vegetable) {
        if(vegetable == null) {
            return null;
        }
        else if (vegetable.equalsIgnoreCase("olive")) {
            return new Olive();
        }
        else if (vegetable.equalsIgnoreCase("pickles")) {
            return new Pickles();
        }
        else if (vegetable.equalsIgnoreCase("tomato")) {
            return new Tomato();
        }
        return null;
    }

    @Override
    Topping getTopping(String topping) {
        return null;
    }
}
