package creationalPatterns.abstractFactory.vegetable;

public interface Vegetable {
    void choose();
}
