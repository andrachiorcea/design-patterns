package creationalPatterns.abstractFactory.vegetable;

public class Pickles implements Vegetable {

    @Override
    public void choose() {
        System.out.println("You added pickles!");
    }
}
