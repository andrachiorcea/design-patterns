package creationalPatterns.abstractFactory.vegetable;

public class Olive implements Vegetable {

    @Override
    public void choose() {
        System.out.println("You added olives!");
    }
}
