package creationalPatterns.abstractFactory.vegetable;

public class Tomato implements Vegetable{

    @Override
    public void choose() {
        System.out.println("You added tomatoes!");
    }
}
