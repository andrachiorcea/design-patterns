package creationalPatterns.abstractFactory.toppings;

public class Yoghurt implements Topping {

    @Override
    public void pick() {
        System.out.println("You chose to add yogurt");
    }

}
