package creationalPatterns.abstractFactory.toppings;

public interface Topping {
    void pick();
}
