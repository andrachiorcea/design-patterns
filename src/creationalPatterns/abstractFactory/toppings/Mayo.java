package creationalPatterns.abstractFactory.toppings;

public class Mayo implements Topping {

    @Override
    public void pick() {
        System.out.println("You chose to add mayo");
    }
}
