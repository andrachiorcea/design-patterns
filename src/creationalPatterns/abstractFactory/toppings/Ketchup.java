package creationalPatterns.abstractFactory.toppings;

public class Ketchup implements Topping {

    @Override
    public void pick() {
        System.out.println("You chose to add ketchup");
    }
}
