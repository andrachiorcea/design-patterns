package creationalPatterns.builder.meal;

import creationalPatterns.builder.item.ChickenBurger;
import creationalPatterns.builder.item.Fanta;
import creationalPatterns.builder.item.Sprite;
import creationalPatterns.builder.item.VegBurger;

public class MealBuilder {
    public Meal prepareVegMeal (){
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Sprite());
        return meal;
    }

    public Meal prepareNonVegMeal (){
        Meal meal = new Meal();
        meal.addItem(new ChickenBurger());
        meal.addItem(new Fanta());
        return meal;
    }
}
