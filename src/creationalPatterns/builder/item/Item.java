package creationalPatterns.builder.item;

import creationalPatterns.builder.packing.Packing;

public interface Item {
    String name();
    Packing packing();
    float price();
}