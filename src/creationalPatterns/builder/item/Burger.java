package creationalPatterns.builder.item;

import creationalPatterns.builder.packing.Packing;
import creationalPatterns.builder.packing.Wrapper;

public abstract class Burger implements Item{

    @Override
    public Packing packing() {
        return new Wrapper();
    }

    @Override
    public abstract float price();

}
