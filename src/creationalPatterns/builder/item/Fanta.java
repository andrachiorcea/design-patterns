package creationalPatterns.builder.item;

public class Fanta extends Drink {
    @Override
    public String name() {
        return "Fanta";
    }

    @Override
    public float price() {
        return 5f;
    }
}
