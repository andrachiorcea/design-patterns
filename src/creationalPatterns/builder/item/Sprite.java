package creationalPatterns.builder.item;

public class Sprite extends Drink {
    @Override
    public String name() {
        return "Sprite";
    }

    @Override
    public float price() {
        return 5f;
    }
}
