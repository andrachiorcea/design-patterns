package creationalPatterns.builder.item;

public class Cola extends Drink {
    @Override
    public String name() {
        return "Cola";
    }

    @Override
    public float price() {
        return 5f;
    }
}
