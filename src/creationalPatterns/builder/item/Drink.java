package creationalPatterns.builder.item;

import creationalPatterns.builder.packing.Bottle;
import creationalPatterns.builder.packing.Packing;
import creationalPatterns.builder.packing.Wrapper;

public abstract class Drink implements Item {

    @Override
    public Packing packing() {
        return new Bottle();
    }

    @Override
    public abstract float price();
}
