package creationalPatterns.builder.packing;

public interface Packing {
    public String pack();
}
